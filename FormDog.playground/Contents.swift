//: Playground - noun: a place where people can play

import UIKit
import PlaygroundSupport
import FormDog


struct FormDelegate: FormViewControllerDelegate {
  let fields: [Field]
  
  init() {
    let nameField = StringField<StringFieldCell>(title: "Name", value: "Manel", isReadOnly: false)
    nameField.updateHandler = { _ in
      print("name field did update to \(nameField.value)")
    }
    
    let openField = StringField<StringFieldCell>(title: "Open", value: "Quetas", isReadOnly: false)
    openField.updateHandler = { _ in
      print("On field did update to \(openField.value)")
    }
    
    let picker = PickerField<PickerCell<String>>(title: "Opções", value: "Tretas", isReadOnly: false, options: ["Tretas", "Quetas"])
    
    self.fields = [
      nameField,
      openField,
      picker
    ]
  }
  
  func numberOfFieldsForFormViewController(_ controller: FormViewController) -> Int {
    return fields.count
  }
  
  func formViewController(_ controller: FormViewController, fieldAt index: Int) -> Field {
    return fields[index]
  }
  
  func formViewController(_ controller: FormViewController, didPressSubmitWithCompletionHandler completionHandler: @escaping (FormViewController.FormViewControllerSubmitAction) -> Void) {
    return completionHandler(.success)
  }
}

let formVC = FormViewController()
formVC.delegate = FormDelegate()
formVC.title = "Form"

let navController = UINavigationController(rootViewController: formVC)

let window = UIWindow()
window.rootViewController = navController
window.makeKeyAndVisible()

PlaygroundPage.current.liveView = window
PlaygroundPage.current.needsIndefiniteExecution = true
