import Foundation

public struct RequiredRule<T: FieldValue>: FieldValidationRule {
  public init() {
  }
  
  public func validateField(_ field: FieldFor<T>) -> Bool {
    guard let existingValue = field.value else {
      return false
    }
    
    if let collection = existingValue as? String {
      return !collection.isEmpty
    }
    
    return true
  }
}
