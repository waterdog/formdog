import Foundation

public class BoolField<T: TypedCell>: BaseField<T> where T: BaseFieldCell, T.Value == Bool {
}
