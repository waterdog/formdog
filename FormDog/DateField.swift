import Foundation

public class DateTimeField<T: TypedCell>: BaseField<T> where T: BaseFieldCell, T.Value == Date {
  public enum Mode {
    case timeAndDate
    case date
    case timeInterval
  }
  
  public let dateFormatter: DateFormatter
  public let calendar: Calendar
  
  public var minimumDate: Date = Date.distantPast {
    didSet {
      cell.updateContents()
    }
  }
  public var maximumDate: Date = Date.distantFuture {
    didSet {
      cell.updateContents()
    }
  }
  public var mode: Mode = .timeAndDate
  
  public init(title: String, value: T.Value?, isReadOnly readOnly: Bool = false, calendar: Calendar = .current) {
    self.calendar = calendar
    
    let formatter = DateFormatter()
    formatter.calendar = calendar
    formatter.timeZone = calendar.timeZone
    formatter.timeStyle = .medium
    formatter.dateStyle = .medium
    self.dateFormatter = formatter
    
    super.init(title: title, value: value, isReadOnly: readOnly)
  }
}

public class DateField<T: TypedCell>: DateTimeField<T> where T: BaseFieldCell, T.Value == Date {
  public override init(title: String, value: Value?, isReadOnly readOnly: Bool = false, calendar: Calendar = .current) {
    var startOfDay: Date?
    if let value = value {
      startOfDay = Calendar.current.startOfDay(for: value)
    }
    
    super.init(title: title, value: startOfDay, isReadOnly: readOnly, calendar: calendar)
    self.mode = .date
    self.dateFormatter.timeStyle = .none
  }
}

public class TimeIntervalField<T: TypedCell>: BaseField<T> where T: BaseFieldCell, T.Value == TimeInterval {
}
