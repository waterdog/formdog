import UIKit

public protocol FormViewControllerDelegate {
  func titleForFormViewController(_ controller: FormViewController) -> String?
  func formViewController(_ controller: FormViewController, didPressSubmitWithCompletionHandler completionHandler: @escaping (FormViewController.FormViewControllerSubmitAction) -> Void)
  func formViewControllerDidCancel(_ controller: FormViewController)
}

extension FormViewControllerDelegate {
  public func titleForFormViewController(_ controller: FormViewController) -> String? {
    return nil
  }
  
  public func formViewControllerDidCancel(_ controller: FormViewController) {
    // no-op
  }
}

open class FormViewController: UITableViewController {
  
  public enum FormViewControllerSubmitAction {
    case error, success
  }
  
  enum CellReuseIdentifier: String {
    case textField = "TextFieldCell"
    case picker = "PickerCell"
    case datePicker = "DateCell"
  }
  
  public var fields: [Field] = [] {
    didSet {
      tableView.reloadData()
    }
  }
  var visibleFields: [Field] {
    return fields.filter { !$0.isHidden }
  }
  
  public weak var editButton: UIBarButtonItem!
  @IBOutlet public weak var cancelButton: UIBarButtonItem?
  
  open var delegate: FormViewControllerDelegate?
  
  deinit {
    delegate = nil
  }
  
  override open func viewDidLoad() {
    super.viewDidLoad()
    
    tableView.allowsSelection = true
    tableView.allowsSelectionDuringEditing = true
    tableView.allowsMultipleSelectionDuringEditing = false
    
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = 60
    
    let editButton = makeEditButton()
    self.editButton = editButton
    navigationItem.rightBarButtonItem = editButton
    
    if presentingViewController != nil {
      self.setEditing(true, animated: false)
    }
  }
  
  /**
   *  Entry point to start the form submitting process.
   *
   *  1. Form validation, marking validity of cells in the process;
   *  2. Exit editing mode, if form is valid.
   */
  open func submitForm() {
    let editableFields = visibleFields.filter { !$0.isReadOnly }
    let isFormValid = editableFields.reduce(true) { result, field in
      field.cell.isValid = field.isValid
      return result && field.isValid
    }
    
    if isFormValid {
      setEditing(false, animated: true)
    }
  }
  
  // MARK: - Navigation item actions
  
  private var editingDisablingWasInvokedByCancelling = false
  
  override open func setEditing(_ editing: Bool, animated: Bool) {
    
    if !editing {
      if !editingDisablingWasInvokedByCancelling {
        cancelButton?.isEnabled = false
        
        let completionHandler: (FormViewControllerSubmitAction) -> Void = { action in
          DispatchQueue.main.async {
            switch action {
            case .success:
              self.navigationItem.setLeftBarButton(nil, animated: animated)
              
              self.willChangeValue(forKey: #keyPath(isEditing))
              super.setEditing(false, animated: animated)
              self.didChangeValue(forKey: #keyPath(isEditing))
              
              if self.presentingViewController != nil {
                self.dismiss(animated: true, completion: nil)
              }
            case .error:
              self.cancelButton?.isEnabled = true
            }
          }
        }
        
        delegate?.formViewController(self, didPressSubmitWithCompletionHandler: completionHandler)
      } else {
        navigationItem.setLeftBarButton(nil, animated: animated)
        
        self.willChangeValue(forKey: #keyPath(isEditing))
        super.setEditing(editing, animated: animated)
        self.didChangeValue(forKey: #keyPath(isEditing))
      }
      
      editingDisablingWasInvokedByCancelling = false
    } else {
      let cancelButton = makeCancelButton()
      navigationItem.setLeftBarButton(cancelButton, animated: animated)
      self.cancelButton = cancelButton

      self.willChangeValue(forKey: #keyPath(isEditing))
      super.setEditing(editing, animated: animated)
      self.didChangeValue(forKey: #keyPath(isEditing))
    }
  }
  
  @IBAction public func cancelAction(_ sender: UIBarButtonItem) {
    delegate?.formViewControllerDidCancel(self)
    editingDisablingWasInvokedByCancelling = true
    setEditing(false, animated: true)
    
    if self.presentingViewController != nil {
      dismiss(animated: true, completion: nil)
    }
  }
  
  // MARK: - UIBarButtonItems Generation
  
  open func makeEditButton() -> UIBarButtonItem {
    editButtonItem.target = self
    editButtonItem.action = #selector(editButtonItemAction(_:))
    return editButtonItem
  }
  
  open func makeCancelButton() -> UIBarButtonItem {
    return UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelAction(_:)))
  }
  
  @objc private func editButtonItemAction(_ sender: UIBarButtonItem) {
    if !isEditing {
      setEditing(true, animated: true)
    } else {
      submitForm()
    }
  }
  
  // MARK: - Table view data source
  
  override final public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return visibleFields.count
  }
  
  override final public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let field = visibleFields[indexPath.row]
    field.cell.untypedField = field
    field.cell.updateContents()
    return field.cell
  }
  
  override final public func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    let field = visibleFields[indexPath.row]
    return !field.isReadOnly
  }
  
  override final public func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
    return .none
  }
  
  override final public func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
    return false
  }
  
  // MARK: - UITableViewDelegate
  
  open override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    visibleFields[indexPath.row].didSelect()
    tableView.deselectRow(at: indexPath, animated: true)
  }
}
