import Foundation

public final class PickerField<T: TypedCell>: BaseField<T> where T: BaseFieldCell {
  public typealias Value = T.Value
  public var options: [Value]
  
  public init(title: String, value: Value?, isReadOnly readOnly: Bool, options: [Value]) {
    self.options = options
    
    super.init(title: title, value: value, isReadOnly: readOnly)
  }
  
  
}
