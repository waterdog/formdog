import Foundation

public protocol FieldValidation {
}

public protocol FieldValidationRule: FieldValidation {
  associatedtype Value: FieldValue
  func validateField(_ field: FieldFor<Value>) -> Bool
}

public struct AnyValidationRule<T: FieldValue>: FieldValidationRule {
  private let originalValidation: (FieldFor<T>) -> Bool
  
  init<U: FieldValidationRule>(rule: U) where U.Value == T {
    self.originalValidation = rule.validateField
  }
  
  public func validateField(_ field: FieldFor<T>) -> Bool {
    return originalValidation(field)
  }
}
