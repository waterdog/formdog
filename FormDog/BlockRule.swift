import Foundation

public struct BlockRule<T: FieldValue>: FieldValidationRule {
  let validationBlock: (FieldFor<T>) -> Bool
  
  public init(block: @escaping (FieldFor<T>) -> Bool) {
    self.validationBlock = block
  }
  
  public func validateField(_ field: FieldFor<T>) -> Bool {
    return validationBlock(field)
  }
}
