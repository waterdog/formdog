import Foundation

public final class StringField<T: TypedCell>: BaseField<T> where T: BaseFieldCell, T.Value == String {
}
