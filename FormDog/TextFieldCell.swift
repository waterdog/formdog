import Foundation

public class TextFieldCell<T: FieldValue>: FieldCell<T>, UITextFieldDelegate {
  @IBOutlet public var titleLabel: UILabel!
  @IBOutlet public var textField: UITextField!
  
  public required init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    
    self.titleLabel = UILabel()
    titleLabel.setContentCompressionResistancePriority(.required, for: .horizontal)
    titleLabel.setContentHuggingPriority(.required, for: .horizontal)
    
    self.textField = UITextField()
    textField.delegate = self
    textField.textAlignment = .right
    
    let stackView = UIStackView(arrangedSubviews: [titleLabel, textField])
    stackView.spacing = 8
    stackView.translatesAutoresizingMaskIntoConstraints = false
    self.addSubview(stackView)
    
    let views = ["stackView": stackView]
    var constraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-[stackView]-|", options: [], metrics: nil, views: views)
    constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-[stackView]-|", options: [], metrics: nil, views: views)
    NSLayoutConstraint.activate(constraints)
  }
  
  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override public func setEditing(_ editing: Bool, animated: Bool) {
    textField.isEnabled = editing
    textField.textColor = editing ? .black : .darkGray
    super.setEditing(editing, animated: animated)
  }
  
  public override func updateContents() {
    titleLabel.text = field.title
    textField.text = String(describing: field.value)
  }
  
  override public func didSelect() {
    super.didSelect()
    textField.becomeFirstResponder()
  }
  
  public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    return true
  }
}
