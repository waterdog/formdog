import UIKit

public class PickerCell<T: FieldValue>: TextFieldCell<T>, UIPickerViewDataSource, UIPickerViewDelegate {
  let pickerView: UIPickerView
  
  override public var field: FieldFor<T>! {
    didSet {
      self.pickerField = field as! PickerField<PickerCell<T>>
    }
  }
  
  var pickerField: PickerField<PickerCell<T>>!
  
  required public init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    self.pickerView = UIPickerView()

    super.init(style: .default, reuseIdentifier: nil)
        
    pickerView.dataSource = self
    pickerView.delegate = self
    textField.inputView = pickerView
    
    let stackView = UIStackView(arrangedSubviews: [titleLabel, textField])
    stackView.spacing = 8
    stackView.translatesAutoresizingMaskIntoConstraints = false
    self.addSubview(stackView)
    
    let views = ["stackView": stackView]
    var constraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-[stackView]-|", options: [], metrics: nil, views: views)
    constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-[stackView]-|", options: [], metrics: nil, views: views)
    NSLayoutConstraint.activate(constraints)
  }
  
  required public init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    if let row = pickerField.options.index(of: pickerField.value!) {
      pickerView.selectRow(row, inComponent: 0, animated: false)
    }
    return true
  }

  public func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
  }
  
  public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return pickerField.options.count
  }
  
  public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return String(describing: pickerField.options[row])
  }
  
  public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    let selectedValue = pickerField.options[row]
    textField.text = String(describing: selectedValue)
    field.value = selectedValue
  }

  func textFieldDidEndEditing(_ textField: UITextField) {
  }
}
