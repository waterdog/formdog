import UIKit

public class DateCell: TextFieldCell<Date> {
  let datePicker: UIDatePicker
  
  required public init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    self.datePicker = UIDatePicker()
    datePicker.datePickerMode = .date
    
    super.init(style: .default, reuseIdentifier: nil)
        
    datePicker.addTarget(self, action: #selector(pickerAction(_:)), for: .valueChanged)
    textField.inputView = datePicker
    
    let stackView = UIStackView(arrangedSubviews: [titleLabel, textField])
    stackView.spacing = 8
    stackView.translatesAutoresizingMaskIntoConstraints = false
    self.addSubview(stackView)
    
    let views = ["stackView": stackView]
    var constraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-[stackView]-|", options: [], metrics: nil, views: views)
    constraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-[stackView]-|", options: [], metrics: nil, views: views)
    NSLayoutConstraint.activate(constraints)
  }

  required public init(coder aDecoder: NSCoder) {
    fatalError("Not implemented")
  }
  
  // MARK: Date Picker Action

  @objc fileprivate func pickerAction(_ sender: UIDatePicker) {
    textField.text = String(describing: sender.date)
    field.value = sender.date
  }

  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    if let date = field.value {
      datePicker.setDate(date, animated: false)
    }
    return true
  }
  
  func textFieldDidEndEditing(_ textField: UITextField) {
  }
}
