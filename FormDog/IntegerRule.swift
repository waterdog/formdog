import Foundation

public struct IntegerRule: FieldValidationRule {
  public init() {
  }
  
  public func validateField(_ field: FieldFor<String>) -> Bool {
    guard let value = field.value, !value.isEmpty else {
      return true
    }
    return Int(value) != nil
  }
}
