import Foundation

public typealias FieldValue = Equatable & CustomStringConvertible

public protocol Field: class, CustomDebugStringConvertible {
  var title: String { get }
  var displayValue: String { get }
  var isReadOnly: Bool { get }
  var isHidden: Bool { get }
  var cell: BaseFieldCell! { get }
  
  /**
   * Called when its cell is selected on `FormViewController`. This method will also call the method `didSelect()` on the cell.
   *
   * Subclasses overriding this method must call `super`.
   */
  func didSelect()
  
  /**
   * Returns `true` if the field's value is valid; `false` otherwise.
   */
  var isValid: Bool { get }
  
}

extension Field {
  public var debugDescription: String {
    return "<\(type(of: self))> \"\(title)\""
  }
}

public protocol TypedField: Field {
  associatedtype Value: FieldValue
  var value: Value? { get set }
  
  /**
   *  Validation Rules
   */
  var validationRules: [AnyValidationRule<Value>] { get set }
  
  func addValidationRule<Rule: FieldValidationRule>(_ rule: Rule) where Rule.Value == Value
}

extension TypedField {
  public var displayValue: String {
    return String(describing: value)
  }
}
