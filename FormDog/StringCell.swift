import UIKit

public class StringFieldCell: TextFieldCell<String> {
  
  override public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    let resultingString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
    field.value = resultingString
    return true
  }
  
}
