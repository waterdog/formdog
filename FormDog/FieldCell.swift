import UIKit

open class BaseFieldCell: UITableViewCell {
  open func updateContents() {}
  var untypedField: Field!
  
  public var formViewController: FormViewController? {
    var nextResponder: UIResponder? = self
    while nextResponder != nil {
      nextResponder = nextResponder?.next
      if let formVC = nextResponder as? FormViewController {
        return formVC
      }
    }
    return nil
  }
  
  public required override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
  }
  
  public required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  open var isValid: Bool = true
  
  /**
   *  Called when the cell was selected by the user
   */
  open func didSelect() {
  }
  
}

public protocol TypedCell {
  associatedtype Value: FieldValue
  var field: FieldFor<Value>! { get set }
}

open class FieldCell<T: FieldValue>: BaseFieldCell, TypedCell {
  override var untypedField: Field! {
    didSet {
      field = untypedField as! FieldFor<T>
    }
  }
  
  open weak var field: FieldFor<T>! {
    didSet {
      updateContents()
    }
  }
}
