import Foundation
import UIKit

public class ModalField<T: TypedCell, U: UIViewController>: BaseField<T> where T: BaseFieldCell {
  
  public var presentationHandler: (() -> U)
  
  public init(title: String, value: Value?, isReadOnly readOnly: Bool = false, presentationHandler: @escaping () -> U) {
    self.presentationHandler = presentationHandler
    
    super.init(title: title, value: value, isReadOnly: readOnly)
  }
  
  override public func didSelect() {
    super.didSelect()
    
    let viewController = presentationHandler()
    cell.formViewController?.present(viewController, animated: true, completion: nil)
  }
}
