import Foundation

open class FieldFor<T: FieldValue>: TypedField {
  /// Title for field identification
  open let title: String

  /// Value that the field currently holds
  open var value: T?
  
  public var isReadOnly: Bool
  
  public var isHidden: Bool = false
  
  public var validationRules: [AnyValidationRule<T>] = []
  
  open var isValid: Bool {
    var valid = true
    validationRules.forEach { valid = valid && $0.validateField(self) }
    return valid
  }
  
  open var cell: BaseFieldCell! {
    return nil
  }
  
  public init(title: String, value: T?, isReadOnly readOnly: Bool = false) {
    self.title = title
    self.value = value
    self.isReadOnly = readOnly
    self.validationRules = [
      AnyValidationRule(rule: RequiredRule<T>())
    ]
  }
  
  public func didSelect() {
  }
  
  public func addValidationRule<Rule>(_ rule: Rule) where Rule : FieldValidationRule, T == Rule.Value {
    let anyRule = AnyValidationRule(rule: rule)
    validationRules.append(anyRule)
  }
}

open class BaseField<T: TypedCell>: FieldFor<T.Value> where T: BaseFieldCell {
  public typealias Value = T.Value
  
  final override public var cell: BaseFieldCell! {
    return typedCell
  }
  public var typedCell: T
  
  open override var value: Value? {
    didSet {
      updateHandler?(self)
    }
  }
  
  // Closures for callbakcs
  open var updateHandler: ((BaseField<T>) -> Void)?
  open var selectionHandler: ((BaseField<T>) -> Void)?
  
  public override init(title: String, value: Value?, isReadOnly readOnly: Bool = false) {
    self.typedCell = T(style: .default, reuseIdentifier: nil)
    
    super.init(title: title, value: value, isReadOnly: readOnly)
    
    typedCell.untypedField = self
  }
  
  public override func didSelect() {
    super.didSelect()
    selectionHandler?(self)
  }
}
