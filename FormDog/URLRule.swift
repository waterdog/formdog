import Foundation

public struct URLRule: FieldValidationRule {
  public init() {
  }
  
  public func validateField(_ field: FieldFor<String>) -> Bool {
    guard let value = field.value, !value.isEmpty else {
        return true
    }
    
    return URL(string: value) != nil
  }
}
